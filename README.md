# TalkK App

### Build instructions
Run command:
```
./gradlew build
```

### Run instructions

Run command:

```
./gradlew bootRun
```

You can start app without gradle. After successful build, run:

```
java -jar build/libs/talkk-app-${project.version}.jar
```

### Useful commands

* `./gradlew build -x test` - fast build without any tests

### Architecture decision records

* 01 - [Error responses in APIs](adr/01_error_responses_in_apis.md)
* 02 - [Pagination in APIs](adr/02_pagination_in_apis.md)
