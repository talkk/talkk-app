## Branching model

* This project follows [GitHub flow](https://guides.github.com/introduction/flow/)
* Branch name should start with project issue number. For example, for issue [#5](https://gitlab.com/talkk/talkk-app/-/issues/5) branch name can be `5-user-registration`
* When your branch is ready open a [Merge Request](https://gitlab.com/talkk/talkk-app/-/merge_requests/new) to the `main`

## Commit convention
This project follows the [ESLint Convention](https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-eslint). It means that commit message format is as follows:

```
Tag: Short description (fixes #1234)

Longer description here if necessary
```

The first line of the commit message (the summary) must have a specific format.

The `Tag` is one of the following:

* `Fix` - for a bug fix.
* `Update` - either for a backwards-compatible enhancement or for a rule change that adds reported problems.
* `New` - implemented a new feature.
* `Breaking` - for a backwards-incompatible enhancement or feature.
* `Docs` - changes to documentation only.
* `Build` - changes to build process only.
* `Upgrade` - for a dependency upgrade.
* `Chore` - for refactoring, adding tests, etc. (anything that isn't user-facing).

The message summary should be a one-sentence description of the change, and it must be 72 characters in length or shorter. If the pull request addresses an issue, then the issue number should be mentioned at the end. If the commit doesn't completely fix the issue, then use `(refs #1234)` instead of `(fixes #1234)`.

## IDE configuration

IntelliJ IDEA is a preferred IDE for development of this project. If you're using different tool
please follow manual instructions below.

### Copyright setup

Copyright config is already committed to the repository in the `.idea/copyright` directory.
Intellij should take a configuration after first opening project automatically.

Remember to have checkbox "Update copyright" in commit window active!

#### Manual instruction

Go to `Copyright` settings. Add a new Copyright Profile with text:

```
Copyright (C) ${today.year} the original author or authors.

Use of this source code is governed by an MIT-style
license that can be found in the LICENSE file or at
https://opensource.org/licenses/MIT.
```

Set:

* `Regexp to detext copyright in comments` to `Copyright`
* `Allow replacing copyright if old copyright matches` to `Copyright`

it will update copyright every year continuously.

Apply profile to the Kotlin files only.

### Code style setup

Code style config is already committed to the repository in the `.idea/codeStyles` directory.
Intellij should take a configuration after first opening project automatically.

Remember to have checkbox "Reformat code" in commit window active!

TalkK's code style is based
on [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html)
and [Google Kotlin Style Guide](https://developer.android.com/kotlin/style-guide). 

#### Manual instruction

1. Download XML
   from [here](https://github.com/google/styleguide/blob/gh-pages/intellij-java-google-style.xml).
   Go to `Code Style` settings and import scheme.
1. For Kotlin choose "Set from..." and click "Kotlin style guide"
