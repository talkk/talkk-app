/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.users

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import tech.talkk.users.domain.UserUseCases
import tech.talkk.users.domain.ports.UserRepository

@Configuration
class UsersModuleConfiguration {

    @Bean
    fun userUseCases(userRepository: UserRepository): UserUseCases = UserUseCases(userRepository)
}