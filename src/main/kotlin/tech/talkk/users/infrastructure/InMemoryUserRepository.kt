/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.users.infrastructure

import org.springframework.stereotype.Repository
import tech.talkk.users.domain.model.Email
import tech.talkk.users.domain.model.User
import tech.talkk.users.domain.model.UserId
import tech.talkk.users.domain.model.Username
import tech.talkk.users.domain.ports.UserRepository
import java.time.Instant

@Repository
class InMemoryUserRepository : UserRepository {

    private val users = HashMap<UserId, User>()

    override suspend fun findOrderByDescCreatedAt(createdAtBefore: Instant?, limit: Int): List<User> =
        users.values.sortedByDescending { it.createdAt }
            .filter { user -> createdAtBefore?.let { user.createdAt.isBefore(it) } ?: true }
            .take(limit)

    override suspend fun findById(userId: UserId): User? = users[userId]

    override suspend fun findIdsByUsernameIgnoreCase(username: Username): List<UserId> =
            users.values.filter { it.username.value.equals(username.value, ignoreCase = true) }
                    .map { it.userId }

    override suspend fun findIdsByEmailIgnoreCase(email: Email): List<UserId> =
            users.values.filter { it.email.value.equals(email.value, ignoreCase = true) }
                    .map { it.userId }

    override suspend fun save(user: User): User {
        users[user.userId] = user
        return user
    }
}