/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.users.application.web

import getErrorResponseDto
import jakarta.validation.constraints.Max
import jakarta.validation.constraints.Min
import jakarta.validation.constraints.Pattern
import kotlinx.coroutines.runBlocking
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import tech.talkk.platform.validation.PropertyError
import tech.talkk.platform.validation.PropertyPath
import tech.talkk.platform.validation.updatePaths
import tech.talkk.platform.web.errors.ErrorResponseDto
import tech.talkk.platform.web.errors.WebErrorException
import tech.talkk.users.domain.model.UserId
import tech.talkk.users.domain.model.UserRegistrationResult
import tech.talkk.users.domain.UserUseCases
import java.time.Instant

@RestController
@Validated
@RequestMapping("/api/v1/users")
class UserResource(private val userUseCases: UserUseCases) {

    @GetMapping
    fun findUsers(@RequestParam("limit") @Min(1) @Max(100) limit: Int?,
                  @RequestParam("cursor") @Pattern(regexp = "^[0-9]+$") cursor: String?):
            PageUsersPublicDto = runBlocking {
        val joinBefore = cursor?.let { Instant.ofEpochSecond(it.toLong()) }
        val users = userUseCases.find(limit ?: 100, joinBefore)
        PageUsersPublicDto(
            parameters = PageParametersUsersPublicDto(
                limit = limit,
                cursor = cursor
            ),
            nextCursor = users.nextCursor?.epochSecond?.toString(),
            content = users.content.map { it.toPublicDto() }
        )
    }

    @GetMapping("/{userId}")
    fun getUser(@PathVariable("userId") userId: String): UserPublicDto = runBlocking {
        userUseCases.findById(UserId(userId))?.toPublicDto() ?: throw UserNotFoundWebError()
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun registerNewUser(@RequestBody request: UserRegistrationDto): UserPublicDto = runBlocking {
        when (val result = userUseCases.registerNewUser(request.toCommand())) {
            is UserRegistrationResult.Success -> result.registeredUser.toPublicDto()
            is UserRegistrationResult.Error -> throw UserRegistrationWebError(result.errors)
        }
    }

    @ExceptionHandler(UserNotFoundWebError::class)
    fun handle(ex: UserNotFoundWebError): ResponseEntity<ErrorResponseDto> {
        return getErrorResponseDto(ex)
    }

    @ExceptionHandler(UserRegistrationWebError::class)
    fun handle(ex: UserRegistrationWebError): ResponseEntity<ErrorResponseDto> {
        return getErrorResponseDto(ex, ex.errors.updatePaths(mapOf(
            PropertyPath("username.value") to PropertyPath("username"),
            PropertyPath("email.value") to PropertyPath("email")
            )))
    }
}

class UserNotFoundWebError : WebErrorException() {
    override val status = HttpStatus.NOT_FOUND
    override val errorCode: String = "UserNotFound"
    override val title: String = "User not found"
}

class UserRegistrationWebError(val errors: List<PropertyError>): WebErrorException() {
    override val status: HttpStatus = HttpStatus.BAD_REQUEST
    override val errorCode: String = "UserRegistrationInvalid"
    override val title: String = "User registration request is not valid"
}