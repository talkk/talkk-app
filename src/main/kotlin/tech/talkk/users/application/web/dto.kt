/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.users.application.web

import tech.talkk.users.domain.model.Email
import tech.talkk.users.domain.model.RegistrationCommand
import tech.talkk.users.domain.model.User
import tech.talkk.users.domain.model.Username
import java.time.Instant


data class PageUsersPublicDto(
        val parameters: PageParametersUsersPublicDto,
        val nextCursor: String?,
        val content: List<UserPublicDto>
)

data class PageParametersUsersPublicDto(
        val limit: Int?,
        val cursor: String?
)

data class UserPublicDto(
        val userId: String,
        val username: String,
        val joinedAt: Instant,
)

fun User.toPublicDto() = UserPublicDto(
        userId = this.userId.value,
        username = this.username.value,
        joinedAt = this.createdAt,
)

data class UserRegistrationDto(
        val username: String?,
        val email: String?,
)

fun UserRegistrationDto.toCommand(): RegistrationCommand = RegistrationCommand(
        username = Username(this.username ?: ""),
        email = Email(this.email ?: ""),
)