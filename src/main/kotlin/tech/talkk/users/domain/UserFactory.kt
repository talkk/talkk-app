/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.users.domain

import tech.talkk.users.domain.model.RegistrationCommand
import tech.talkk.users.domain.model.User
import tech.talkk.users.domain.model.UserId
import java.time.Instant

object UserFactory {

    fun create(
        command: RegistrationCommand,
        userId: UserId = UserId.generateRandom(),
        createdAt: Instant = Instant.now()
    ): User = User(
            userId = userId,
            username = command.username,
            email = command.email,
            createdAt = createdAt,
    )
}