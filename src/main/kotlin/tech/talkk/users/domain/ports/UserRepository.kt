/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.users.domain.ports

import tech.talkk.users.domain.model.Email
import tech.talkk.users.domain.model.User
import tech.talkk.users.domain.model.UserId
import tech.talkk.users.domain.model.Username
import java.time.Instant

interface UserRepository {

    suspend fun findOrderByDescCreatedAt(
        createdAtBefore: Instant?,
        limit: Int
    ): List<User>

    suspend fun findById(userId: UserId): User?

    suspend fun findIdsByUsernameIgnoreCase(username: Username): List<UserId>

    suspend fun findIdsByEmailIgnoreCase(email: Email): List<UserId>

    suspend fun save(user: User): User
}