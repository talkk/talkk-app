/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.users.domain

import tech.talkk.platform.domain.model.CursorPage
import tech.talkk.platform.validation.ValidationResult
import tech.talkk.users.domain.model.*
import tech.talkk.users.domain.ports.UserRepository
import java.time.Instant

class UserUseCases(private val userRepository: UserRepository) {

    suspend fun find(limit: Int,
                     createdAtBefore: Instant? = Instant.now()): CursorPage<User, Instant> =
        userRepository.findOrderByDescCreatedAt(createdAtBefore, limit).let { CursorPage(
                content = it,
                nextCursor = if (it.size == limit) it.last().createdAt else null
            ) }

    suspend fun findById(userId: UserId): User? = userRepository.findById(userId)

    suspend fun registerNewUser(command: RegistrationCommand): UserRegistrationResult =
            when (val result = RegistrationCommandValidator(userRepository).validate(command)) {
                ValidationResult.Success -> {
                    val user = UserFactory.create(command)
                    userRepository.save(user)
                    UserRegistrationResult.Success(user)
                }
                is ValidationResult.Failed -> UserRegistrationResult.Error(result.errors)
            }
}