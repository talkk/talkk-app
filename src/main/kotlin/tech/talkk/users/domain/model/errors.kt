/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.users.domain.model

import tech.talkk.platform.validation.ValidationError

object UsernameAlreadyUsedError : ValidationError {
    override val violationCode: String = "UsernameUniqueness"
    override val message: String = "Provided username is already used by another user"
}

object EmailAlreadyUsedError: ValidationError {
    override val violationCode: String = "EmailUniqueness"
    override val message: String = "Provided email is already used by another user"
}