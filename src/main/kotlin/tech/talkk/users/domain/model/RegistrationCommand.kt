/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.users.domain.model

import tech.talkk.platform.validation.ValidationRule
import tech.talkk.platform.validation.ValidatorBuilder


data class RegistrationCommand(
        val username: Username,
        val email: Email,
) {
    fun validate(usersWithUsername: List<UserId>, usersWithEmail: List<UserId>) =
        ValidatorBuilder<RegistrationCommand>()
            .nest(RegistrationCommand::username, username.validator())
            .nest(RegistrationCommand::email, email.validator())
            .rule(RegistrationCommand::username, UsernameUniquenessRule(usersWithUsername))
            .rule(RegistrationCommand::email, EmailUniquenessRule(usersWithEmail))
            .build().validate(this)
}

data class UsernameUniquenessRule(val usersWithUsername: List<UserId>) :
    ValidationRule<Username> {
    override val validationError = UsernameAlreadyUsedError

    override fun test(value: Username): Boolean = usersWithUsername.isEmpty()
}

data class EmailUniquenessRule(val usersWithEmail: List<UserId>) : ValidationRule<Email> {
    override val validationError = EmailAlreadyUsedError

    override fun test(value: Email): Boolean = usersWithEmail.isEmpty()
}