/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.users.domain.model

import java.time.Instant

data class User(
        val userId: UserId,
        val username: Username,
        val email: Email,
        val createdAt: Instant,
)