/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.users.domain.model

import tech.talkk.platform.validation.*

@JvmInline
value class Username(val value: String) {

    fun validator() = ValidatorBuilder<Username>()
        .minLength(Username::value, 4)
        .maxLength(Username::value, 32)
        .notBlank(Username::value)
        // based on https://stackoverflow.com/a/12019115
        .pattern(Username::value, "^(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]*(?<![_.])\$")
        .build()

    fun validate() = validator().validate(this)

}