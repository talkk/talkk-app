/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.users.domain.model

import tech.talkk.platform.validation.PropertyError


sealed class UserRegistrationResult {
    data class Success(val registeredUser: User) : UserRegistrationResult()

    data class Error(val errors: List<PropertyError>) : UserRegistrationResult()
}