/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.users.domain.model

import tech.talkk.platform.validation.EmailRule
import tech.talkk.platform.validation.ValidatorBuilder

@JvmInline
value class Email(val value: String) {
    fun validator() = ValidatorBuilder<Email>()
        .rule(Email::value, EmailRule())
        .build()

    fun validate() = validator().validate(this)
}