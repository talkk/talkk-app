/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.users.domain

import tech.talkk.platform.validation.ValidationResult
import tech.talkk.users.domain.model.RegistrationCommand
import tech.talkk.users.domain.ports.UserRepository

class RegistrationCommandValidator(private val userRepository: UserRepository) {

    suspend fun validate(command: RegistrationCommand): ValidationResult =
            command.validate(
                    userRepository.findIdsByUsernameIgnoreCase(command.username),
                    userRepository.findIdsByEmailIgnoreCase(command.email),
            )
}