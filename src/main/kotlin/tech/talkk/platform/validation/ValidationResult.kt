/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.platform.validation

sealed class ValidationResult {

    object Success : ValidationResult()

    data class Failed(val errors: List<PropertyError>) : ValidationResult() {
        constructor(error: PropertyError) : this(listOf(error))
    }
}
