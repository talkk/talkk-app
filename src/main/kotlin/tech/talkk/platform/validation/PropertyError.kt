/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.platform.validation

typealias old_PropertyPath = PropertyPath
typealias new_PropertyPath = PropertyPath

data class PropertyError(
    val propertyPath: PropertyPath,
    val validationError: ValidationError,
) {
    fun addPrefixToPropertyPath(name: String): PropertyError {
        return this.copy(
            propertyPath = PropertyPath("$name.${this.propertyPath.value}")
        )
    }
}

fun List<PropertyError>.updatePaths(pathMap: Map<old_PropertyPath, new_PropertyPath>) =
    this.map { it.copy(propertyPath = pathMap[it.propertyPath] ?: it.propertyPath) }