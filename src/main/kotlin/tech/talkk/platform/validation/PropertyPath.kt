/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.platform.validation

@JvmInline
value class PropertyPath(val value: String) : Comparable<PropertyPath> {

    override fun compareTo(other: PropertyPath): Int = this.value.compareTo(other.value)
}