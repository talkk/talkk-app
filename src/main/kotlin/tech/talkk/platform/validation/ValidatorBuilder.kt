/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.platform.validation

import kotlin.reflect.KProperty1

class ValidatorBuilder<T> {
    private val rules = mutableListOf<Pair<KProperty1<T, *>, ValidationRule<*>>>()
    private val nested = mutableListOf<Pair<KProperty1<T, *>, Validator<*>>>()

    fun build(): Validator<T> = DefaultPropertyValidator(rules, nested)

    fun <V> rule(
        property: KProperty1<T, V>,
        propertyRule: ValidationRule<V>
    ): ValidatorBuilder<T> {
        rules.add(Pair(property, propertyRule))
        return this
    }

    fun <V> nest(property: KProperty1<T, V>, validator: Validator<V>): ValidatorBuilder<T> {
        nested.add(Pair(property, validator))
        return this
    }

}