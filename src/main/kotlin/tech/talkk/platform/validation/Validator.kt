/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.platform.validation

import kotlin.reflect.KProperty1

interface Validator<T> {

    fun validate(value: T): ValidationResult

}

internal class DefaultPropertyValidator<T>(
    private val rules: MutableList<Pair<KProperty1<T, *>, ValidationRule<*>>>,
    private val nested: MutableList<Pair<KProperty1<T, *>, Validator<*>>>
) : Validator<T> {

    @Suppress("UNCHECKED_CAST")
    override fun validate(value: T): ValidationResult {
        val ruleErrors = rules.mapNotNull {
            val (property, rule) = it
            process(value, property, rule as ValidationRule<Any?>)
        }
        val nestedErrors = nested.flatMap {
            val (property, nest) = it
            process(value, property, nest as Validator<Any?>)
        }
        val allErrors = (ruleErrors + nestedErrors)
            .sortedBy { it.propertyPath }
        return if (allErrors.isEmpty()) {
            ValidationResult.Success
        } else {
            ValidationResult.Failed(allErrors)
        }
    }

    private fun <V> process(
        value: T,
        property: KProperty1<T, V>,
        validator: Validator<V>
    ): List<PropertyError> = when (val result = validator.validate(property.get(value))) {
        ValidationResult.Success -> emptyList()
        is ValidationResult.Failed -> result.errors.map { it.addPrefixToPropertyPath(property.name) }
    }

    private fun <V> process(
        value: T,
        property: KProperty1<T, V>,
        rule: ValidationRule<V>
    ): PropertyError? {
        return if (rule.test(property.get(value))) {
            null
        } else {
            PropertyError(
                propertyPath = PropertyPath(property.name),
                validationError = rule.validationError
            )
        }
    }

}
