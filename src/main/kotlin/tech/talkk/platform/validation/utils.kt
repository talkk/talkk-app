/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.platform.validation

import kotlin.reflect.KProperty1

data class MinLengthError(val min: Int) : ValidationError {
    override val violationCode: String = "MinLength"
    override val message: String = "Length must be greater than or equal to $min"
}

data class MinLengthRule<T : CharSequence>(val min: Int) : ValidationRule<T> {
    override val validationError = MinLengthError(min)

    override fun test(value: T): Boolean = value.length >= min
}

fun <T, V : CharSequence> ValidatorBuilder<T>.minLength(property: KProperty1<T, V>, min: Int) =
    rule(property, MinLengthRule(min))

data class MaxLengthError(val max: Int) : ValidationError {
    override val violationCode: String = "MaxLength"
    override val message: String = "Length must be less than or equal to $max"
}

data class MaxLengthRule<T : CharSequence>(val max: Int) : ValidationRule<T> {
    override val validationError = MaxLengthError(max)

    override fun test(value: T): Boolean = value.length <= max
}

fun <T, V : CharSequence> ValidatorBuilder<T>.maxLength(property: KProperty1<T, V>, max: Int) =
    rule(property, MaxLengthRule(max))

object NotBlankError : ValidationError {
    override val violationCode: String = "NotBlank"
    override val message: String = "Must not be blank"
}

class NotBlankRule<T : CharSequence> : ValidationRule<T> {
    override val validationError = NotBlankError

    override fun test(value: T): Boolean = value.isNotBlank()
}

fun <T, V : CharSequence> ValidatorBuilder<T>.notBlank(property: KProperty1<T, V>) =
    rule(property, NotBlankRule())

data class PatternError(val pattern: String) : ValidationError {
    override val violationCode: String = "Pattern"
    override val message: String = "Must match regular expression: \"${pattern}\""
}

class PatternRule<T : CharSequence>(val pattern: String) : ValidationRule<T> {
    override val validationError = PatternError(pattern)

    override fun test(value: T): Boolean = pattern.toRegex().matches(value)
}

fun <T, V : CharSequence> ValidatorBuilder<T>.pattern(
    property: KProperty1<T, V>,
    pattern: String
) = rule(property, PatternRule(pattern))

object EmailError : ValidationError {
    override val violationCode: String = "Email"
    override val message: String = "Must be a valid email"
}

class EmailRule<T : CharSequence> : ValidationRule<T> {
    override val validationError = EmailError

    private val emailRegex =
        "[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?".toRegex()

    override fun test(value: T): Boolean = emailRegex.matches(value)
}

fun <T, V : CharSequence> ValidatorBuilder<T>.email(
    property: KProperty1<T, V>,
) = rule(property, EmailRule())
