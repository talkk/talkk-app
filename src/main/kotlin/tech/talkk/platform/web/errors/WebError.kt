/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */
package tech.talkk.platform.web.errors

import org.springframework.http.HttpStatus

abstract class WebErrorException : Exception(), WebError

interface WebError {
    val status: HttpStatus
    val errorCode: String
    val title: String

    val type: WebErrorType
        get() = if (status.is5xxServerError) WebErrorType.API_ERROR
        else WebErrorType.INVALID_REQUEST_ERROR
}

enum class WebErrorType(val value: String) {
    API_ERROR("/errors/api-error"),
    INVALID_REQUEST_ERROR("/errors/invalid-request-error")
}