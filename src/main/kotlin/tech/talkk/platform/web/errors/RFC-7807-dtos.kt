/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */
package tech.talkk.platform.web.errors

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import jakarta.validation.constraints.*
import tech.talkk.platform.traceid.TraceIdUtils
import java.time.Instant

@JsonPropertyOrder(value = ["type", "errorCode", "title", "status", "instance", "timestamp", "details"])
data class ErrorResponseDto(
    @field:Min(400)
    @field:Max(599)
    @field:NotNull
    val status: Int, // compliant with RFC 7807

    @field:NotBlank
    val type: String, // compliant with RFC 7807

    @field:NotBlank
    val title: String, // compliant with RFC 7807

    @field:Pattern(regexp = "^([A-Z][a-z0-9]+)((\\d)|([A-Z0-9][a-z0-9]+))*([A-Z])?\$")
    @field:NotNull
    val errorCode: String, // internal RFC 7807 extension

    @field:NotNull
    val timestamp: Instant, // internal RFC 7807 extension

    val details: List<ErrorDetailDto>, // internal RFC 7807 extension
) {

    @field:Pattern(regexp = "^urn:uuid:.+$")
    @field:NotNull
    val instance: String =
        "urn:uuid:${TraceIdUtils.getCurrentTraceId() ?: "about:blank"}" // compliant with RFC 7807
}

@JsonPropertyOrder(value = ["property", "violationCode", "title", "context"])
data class ErrorDetailDto(
    val property: String?,
    val violationCode: String?,
    @field:NotNull
    val title: String,
    val context: Map<String, Any?>? = emptyMap()
)
