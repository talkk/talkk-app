/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import org.springframework.http.ResponseEntity
import tech.talkk.platform.validation.PropertyError
import tech.talkk.platform.validation.ValidationError
import tech.talkk.platform.web.errors.ErrorDetailDto
import tech.talkk.platform.web.errors.ErrorResponseDto
import tech.talkk.platform.web.errors.WebError
import java.time.Instant
import kotlin.reflect.KClass
import kotlin.reflect.full.memberProperties

fun PropertyError.toErrorDetailDto() = ErrorDetailDto(
    property = this.propertyPath.value,
    violationCode = validationError.violationCode,
    title = validationError.message,
    context = toMap(validationError)
        .minus(ValidationError::message.name)
        .minus(ValidationError::violationCode.name),
)

fun getErrorResponseDto(webError: WebError): ResponseEntity<ErrorResponseDto> =
    getErrorResponseDto(webError, emptyList())

fun getErrorResponseDto(
    webError: WebError,
    propertyErrors: List<PropertyError>
): ResponseEntity<ErrorResponseDto> {
    val errorResponse = createFrom(webError, propertyErrors.map { it.toErrorDetailDto() })
    return ResponseEntity(errorResponse, webError.status)
}

private fun createFrom(webError: WebError, details: List<ErrorDetailDto>) =
    ErrorResponseDto(
        type = webError.type.value,
        status = webError.status.value(),
        errorCode = webError.errorCode,
        title = webError.title,
        timestamp = Instant.now(),
        details = details,
    )

@Suppress("UNCHECKED_CAST")
private fun <T : Any> toMap(obj: T): Map<String, Any?> {
    return (obj::class as KClass<T>).memberProperties.associate { prop ->
        prop.name to prop.get(obj)?.let { value ->
            if (value::class.isData) {
                toMap(value)
            } else {
                value
            }
        }
    }
}