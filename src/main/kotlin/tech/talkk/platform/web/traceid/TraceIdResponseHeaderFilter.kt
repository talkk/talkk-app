/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.platform.web.traceid

import jakarta.servlet.FilterChain
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.filter.OncePerRequestFilter
import tech.talkk.platform.traceid.TraceIdUtils.cleanTraceId
import tech.talkk.platform.traceid.TraceIdUtils.generateNewTraceId
import tech.talkk.platform.traceid.TraceIdUtils.getCurrentTraceId

internal class TraceIdResponseHeaderFilter : OncePerRequestFilter() {

    private val log: Logger = LoggerFactory.getLogger(this.javaClass)

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        response.addHeader("trace-id", generateNewTraceId())
        log.debug("Processing of request with traceId {} started", getCurrentTraceId())
        filterChain.doFilter(request, response)
        log.debug("Processing of request with traceId {} finished", getCurrentTraceId())
        cleanTraceId()
    }

}