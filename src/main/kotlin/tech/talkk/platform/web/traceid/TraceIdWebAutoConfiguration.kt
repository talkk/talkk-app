/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.platform.web.traceid

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@ConditionalOnProperty(prefix = "talkk.platform.web", name = ["trace-id-response-header-enabled"], havingValue = "true")
internal class TraceIdWebAutoConfiguration {

    @Bean
    fun traceIdResponseHeaderFilter() = TraceIdResponseHeaderFilter()
}