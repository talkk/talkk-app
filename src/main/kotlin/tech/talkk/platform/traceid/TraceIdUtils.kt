/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.platform.traceid

import org.slf4j.MDC
import java.util.*

private const val TRACE_ID_KEY = "traceId"

object TraceIdUtils {

    internal fun generateNewTraceId(): String = UUID.randomUUID().toString()
            .also { MDC.put(TRACE_ID_KEY, it) }

    fun getCurrentTraceId(): String? = MDC.get(TRACE_ID_KEY)

    internal fun cleanTraceId() {
        MDC.remove(TRACE_ID_KEY)
    }
}