/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.users.application.web

import io.restassured.module.mockmvc.RestAssuredMockMvc
import org.springframework.http.MediaType
import tech.talkk.AbstractBaseResourceIT

class UserResourceIT : AbstractBaseResourceIT() {

    @Test
    fun `Register user successfully`() {
        RestAssuredMockMvc.given()
            .body(UserRegistrationDto("user", "foo@bar.com"))
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .When().post("/api/v1/users")
            .then().statusCode(201)
    }
}
