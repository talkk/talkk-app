/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.users.domain

import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeInstanceOf
import tech.talkk.platform.domain.model.CursorPage
import tech.talkk.users.domain.model.*
import tech.talkk.users.infrastructure.InMemoryUserRepository

class UserUseCasesTest : FunSpec({

    val userUseCases = UserUseCases(InMemoryUserRepository())

    test("Finding by id should return null when there is no user with given userId") {
        userUseCases.findById(UserId("not-exist")) shouldBe null
    }


    test("Happy scenario with registration of the valid user") {
        userUseCases.find(limit = 100) shouldBe CursorPage(emptyList(), null)

        val result = userUseCases.registerNewUser(
            RegistrationCommand(
                Username("user"),
                Email("foo@bar.com")
            )
        )
        result.shouldBeInstanceOf<UserRegistrationResult.Success>()
        result.registeredUser.username shouldBe Username("user")
        result.registeredUser.email shouldBe Email("foo@bar.com")

        userUseCases.findById(result.registeredUser.userId) shouldBe result.registeredUser
        userUseCases.find(limit = 100) shouldBe CursorPage(listOf(result.registeredUser), null)
    }

})
