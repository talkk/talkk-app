/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.users.domain.model

import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeInstanceOf
import tech.talkk.platform.validation.ValidationResult

class EmailTest : FunSpec({

    listOf(
        "simple@example.com",
        "very.common@example.com",
        "abc@example.co.uk",
        "disposable.style.email.with+symbol@example.com",
        "other.email-with-hyphen@example.com",
        "fully-qualified-domain@example.com",
        "user.name+tag+sorting@example.com",
        "example-indeed@strange-example.com",
        "example-indeed@strange-example.inininini",
        "1234567890123456789012345678901234567890123456789012345678901234+x@example.com",
        "email@123.123.123.123",
        "_______@example.com",
    ).map { Email(it) }.forEach {
        test("${it.value} should be a valid email") {
            it.validate().shouldBe(ValidationResult.Success)
        }
    }

    listOf(
        "",
        " ",
        "foo@@bar.com",
        "foo@a@bar.com ",
        " foo@bar.com",
        "foo@bar.com ",
        "plainaddress",
        "#@%^%#$@#$@#.com",
        "@example.com",
        "Joe Smith <email@example.com>",
        "email.example.com",
        ".email@example.com",
        "email.@example.com",
        "email..email@example.com",
        "あいうえお@example.com",
        "email@example.com (Joe Smith)",
        "email@-example.com",
        "email@example..com",
        "Abc..123@example.com",
        "”(),:;<>[\\]@example.com",
        "this\\ is\"really\"not\\allowed@example.com"
    ).map { Email(it) }.forEach {
        test("${it.value} should be a non valid email") {
            it.validate().shouldBeInstanceOf<ValidationResult.Failed>()
        }
    }


})
