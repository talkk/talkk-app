/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.users.domain.model

import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeInstanceOf
import tech.talkk.platform.validation.ValidationResult

class UsernameTest : FunSpec({

    listOf(
        "user",
        "user_2",
        "user.2",
        "1234",
        "1.34",
        "12_4",
        "12345678901234567890123456789012",
        "a_b_c_d_e",
        "a.b.c.d.e"

    ).map { Username(it) }.forEach {
        test("${it.value} should be a valid username") {
            it.validate().shouldBe(ValidationResult.Success)
        }
    }

    listOf(
        "",
        " ",
        "     ",
        "a",
        "ab",
        "abc",
        "123456789012345678901234567890123",
        "_username",
        ".username",
        "username_",
        "username.",
        "user__name",
        "user_.name",
        "user._name",
        "user..name",
        "user___name",
        "user...name",
        "user@foo.com",
        "username ",
        "username+2",
    ).map { Username(it) }.forEach {
        test("${it.value} should be a non valid username") {
            it.validate().shouldBeInstanceOf<ValidationResult.Failed>()
        }
    }


})
