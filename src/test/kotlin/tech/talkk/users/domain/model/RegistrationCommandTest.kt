/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk.users.domain.model

import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import tech.talkk.platform.validation.*

class RegistrationCommandTest : FunSpec({

    test("should be valid when valid not used username and email provided") {
        RegistrationCommand(Username("user"), Email("foo@bar.com"))
            .validate(emptyList(), emptyList()) shouldBe ValidationResult.Success
    }

    test("should be not valid when too short username provided") {
        RegistrationCommand(Username("use"), Email("foo@bar.com"))
            .validate(emptyList(), emptyList()) shouldBe ValidationResult.Failed(
            PropertyError(
                PropertyPath("username.value"), MinLengthError(4)
            )
        )
    }

    test("should be not valid when too long username provided") {
        RegistrationCommand(Username("username_1username_2username_3123"), Email("foo@bar.com"))
            .validate(emptyList(), emptyList()) shouldBe ValidationResult.Failed(
            PropertyError(
                PropertyPath("username.value"), MaxLengthError(32)
            )
        )
    }

    test("should be not valid when invalid email provided") {
        RegistrationCommand(Username("user"), Email("foo@@bar.com"))
            .validate(emptyList(), emptyList()) shouldBe ValidationResult.Failed(
            PropertyError(
                PropertyPath("email.value"), EmailError
            )
        )
    }

    test("should be not valid when username already exists") {
        RegistrationCommand(Username("username"), Email("foo@bar.com"))
            .validate(listOf(UserId("1")), emptyList()) shouldBe ValidationResult.Failed(
            PropertyError(
                PropertyPath("username"),
                UsernameAlreadyUsedError
            )
        )
    }

    test("should be not valid when email already exists") {
        RegistrationCommand(Username("username"), Email("foo@bar.com"))
            .validate(emptyList(), listOf(UserId("1"))) shouldBe ValidationResult.Failed(
            PropertyError(
                PropertyPath("email"),
                EmailAlreadyUsedError
            )
        )
    }

})
