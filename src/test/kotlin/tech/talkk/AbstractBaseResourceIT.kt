/*
 * Copyright (C) 2023 the original author or authors.
 *
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

package tech.talkk

import io.kotest.core.config.AbstractProjectConfig
import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.extensions.spring.SpringExtension
import io.restassured.config.EncoderConfig
import io.restassured.module.mockmvc.RestAssuredMockMvc
import io.restassured.module.mockmvc.config.RestAssuredMockMvcConfig
import io.restassured.module.mockmvc.specification.MockMvcRequestAsyncSender
import io.restassured.module.mockmvc.specification.MockMvcRequestSpecification
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.servlet.MockMvc

@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = [TalkkAppApplication::class]
)
@AutoConfigureMockMvc
abstract class AbstractBaseResourceIT : AnnotationSpec() {

    @Autowired
    lateinit var mockMvc: MockMvc

    @BeforeAll
    fun init() {
        RestAssuredMockMvc.mockMvc(mockMvc)
        RestAssuredMockMvc.config = RestAssuredMockMvcConfig().encoderConfig(
            EncoderConfig.encoderConfig()
                .appendDefaultContentCharsetToContentTypeIfUndefined(false)
        )
    }

    fun MockMvcRequestSpecification.When(): MockMvcRequestAsyncSender {
        return this.`when`()
    }

}

class ProjectConfig : AbstractProjectConfig() {
    override fun extensions() = listOf(SpringExtension)
}